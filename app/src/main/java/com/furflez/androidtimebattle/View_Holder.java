package com.furflez.androidtimebattle;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by felipeamaral on 31/05/16.
 */
public class View_Holder extends RecyclerView.ViewHolder {

    TextView texViewName;
    ProgressBar progressBar;
    CircleImageView circleImageViewPersonagem;

    View_Holder(View itemView) {
        super(itemView);
        texViewName = (TextView) itemView.findViewById(R.id.textViewName);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        circleImageViewPersonagem = (CircleImageView) itemView.findViewById(R.id.circleImageViewPersonagem);
    }

}
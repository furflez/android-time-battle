package com.furflez.androidtimebattle;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonagemListAdapter extends RecyclerView.Adapter<View_Holder> {

    List<Personagem> list = Collections.emptyList();
    Context context;

    public PersonagemListAdapter(ArrayList<Personagem> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.personagemcardadapter, parent, false);
        View_Holder holder = new View_Holder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.texViewName.setText(list.get(position).getNome());
        holder.progressBar.setProgress(list.get(position).getPosicao());
        holder.progressBar.setMax(list.size()+1);
        holder.circleImageViewPersonagem.setImageDrawable(list.get(position).getImagem());

        //animate(holder);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(Personagem personagem) {
        list.add(personagem);
        notifyItemInserted(list.indexOf(personagem));
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Personagem personagem) {
        int position = list.indexOf(personagem);
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void update(Personagem personagem){
        int i = list.indexOf(personagem);
        personagem.setPosicao(personagem.getPosicao()+1);
        list.set(i, personagem);
        notifyItemChanged(i);
    }

}
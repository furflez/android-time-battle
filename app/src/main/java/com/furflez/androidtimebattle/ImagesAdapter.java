package com.furflez.androidtimebattle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by felipeamaral on 01/06/16.
 */
public class ImagesAdapter extends BaseAdapter {
    ArrayList<Integer> images;
    private Context context;

    // Gets the context so it can be used later
    public ImagesAdapter(Context c, ArrayList<Integer> images) {
        this.images = images;
        context = c;
    }

    // Total number of things contained within the adapter
    public int getCount() {
        return images.size();
    }

    // Require for structure, not really used in my code.
    public Object getItem(int position) {
        return null;
    }

    // Require for structure, not really used in my code. Can
    // be used to get the id of an item in the adapter for
    // manual control.
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position,
                        View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.listadapter_imagens, parent, false);
        CircleImageView circleImageView = (CircleImageView) rowView.findViewById(R.id.circleImageViewPersonagem);
        circleImageView.setImageResource(images.get(position));

        return rowView;
    }
}
package com.furflez.androidtimebattle;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    ArrayList<Personagem> personagens = new ArrayList<>();

    LinearLayout linearLayout;
    RecyclerView recyclerView;
    CircleImageView circleImageView;
    TextView textViewNome;
    Button buttonProximo;
    PersonagemListAdapter personagemListAdapter;


    boolean infinito = false;
    boolean iniciado = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        linearLayout = (LinearLayout) findViewById(R.id.linearLayoutVez);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        buttonProximo = (Button) findViewById(R.id.button);
        circleImageView = (CircleImageView) findViewById(R.id.circleImageViewPersonagem);
        textViewNome = (TextView) findViewById(R.id.textViewName);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addDialog();
            }
        });

        buttonProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateATB();
            }
        });
    }

    private void update() {
        if (personagens.size() > 0 && !iniciado) {
            final Button buttonIniciar = (Button) findViewById(R.id.buttonIniciar);
            buttonIniciar.setEnabled(true);
            buttonIniciar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    turnosConfigDialog();
                    iniciado = true;
                    ((TextView) findViewById(R.id.textViewInstrucao)).setVisibility(View.GONE);
                    buttonIniciar.setVisibility(View.GONE);
                }
            });
        }
        if (iniciado) {
            linearLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);

            personagemListAdapter = new PersonagemListAdapter(personagens, getApplicationContext());
            recyclerView.setAdapter(personagemListAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }


    private void ordenar() {

        Collections.sort(personagens, new CustomComparator());

    }

    public class CustomComparator implements Comparator<Personagem> {
        @Override
        public int compare(Personagem p1, Personagem p2) {
            return p2.getIniciativa() - p1.getIniciativa();
        }
    }

    private void updateATB() {
        if (personagens.size() != 0) {
            if (!infinito) {
                Personagem personagem = personagens.get(0);
                personagemListAdapter.remove(personagem);
                circleImageView.setImageDrawable(personagem.getImagem());
                textViewNome.setText("Vez de: " + personagem.getNome());

                for (Personagem p : personagens) {
                    personagemListAdapter.update(p);

                }

            } else {
                Personagem personagem = personagens.get(0);
                personagemListAdapter.remove(personagem);
                circleImageView.setImageDrawable(personagem.getImagem());
                textViewNome.setText("Vez de: " + personagem.getNome());
                personagem.setPosicao(0);
                personagemListAdapter.insert(personagem);
            }
        } else {
            iniciado = false;
            linearLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            ((TextView) findViewById(R.id.textViewInstrucao)).setVisibility(View.VISIBLE);
            ((Button) findViewById(R.id.buttonIniciar)).setVisibility(View.VISIBLE);
            ((Button) findViewById(R.id.buttonIniciar)).setEnabled(false);
        }
    }

    CircleImageView dialogCircleImageViewPersonagem;

    private void addDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_addpersonagem);

        final EditText editTextNome = (EditText) dialog.findViewById(R.id.editTextNome);
        final EditText editTextNumero = (EditText) dialog.findViewById(R.id.editTextNumero);
        dialogCircleImageViewPersonagem = (CircleImageView) dialog.findViewById(R.id.circleImageViewPersonagem);
        Button buttonConfirmar = (Button) dialog.findViewById(R.id.buttonConfirmar);
        dialog.show();

        dialogCircleImageViewPersonagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagemListDialog();
            }
        });

        buttonConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Personagem personagem = new Personagem();
                personagem.setNome(editTextNome.getText().toString());
                personagem.setIniciativa(Integer.parseInt(editTextNumero.getText().toString()));
                personagem.setImagem(dialogCircleImageViewPersonagem.getDrawable());
                personagens.add(personagem);
                update();
                dialog.dismiss();
            }
        });
    }

    private void imagemListDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_imagemlist);

        final GridView gridView = (GridView) dialog.findViewById(R.id.gridView);

        final ArrayList<Integer> images = new ArrayList<>();

        images.add(R.drawable.face01);
        images.add(R.drawable.face02);
        images.add(R.drawable.face03);
        images.add(R.drawable.face04);
        images.add(R.drawable.face05);
        images.add(R.drawable.face06);
        images.add(R.drawable.face07);
        images.add(R.drawable.face08);
        images.add(R.drawable.face09);
        images.add(R.drawable.face10);
        images.add(R.drawable.face11);
        images.add(R.drawable.face12);
        images.add(R.drawable.face13);
        images.add(R.drawable.face14);
        images.add(R.drawable.face15);
        images.add(R.drawable.face16);
        images.add(R.drawable.face17);
        images.add(R.drawable.face18);
        images.add(R.drawable.face19);
        images.add(R.drawable.face20);
        images.add(R.drawable.face21);

        images.add(R.drawable.monster00);
        images.add(R.drawable.monster01);
        images.add(R.drawable.monster02);
        images.add(R.drawable.monster03);
        images.add(R.drawable.monster04);
        images.add(R.drawable.monster05);
        images.add(R.drawable.monster06);
        images.add(R.drawable.monster07);
        ;
        ImagesAdapter adapter = new ImagesAdapter(getApplicationContext(), images);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                dialogCircleImageViewPersonagem.setImageResource(images.get(position));
            }
        });


        dialog.show();
    }

    private void turnosConfigDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_turnosconfig);

        final TextView textView = (TextView) dialog.findViewById(R.id.textView);
        final SeekBar seekBar = (SeekBar) dialog.findViewById(R.id.seekBar);
        final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.checkBox);
        Button button = (Button) dialog.findViewById(R.id.buttonConfirmar);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                seekBar.setEnabled(isChecked);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setText("" + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ordenar();

                ArrayList<Personagem> tempArrayList = new ArrayList<Personagem>();
                if (!checkBox.isChecked()) {
                    for (int i = 0; i < seekBar.getProgress(); i++) {
                        for (Personagem personagem : personagens) {
                            Personagem p = new Personagem();
                            p.setNome(personagem.getNome());
                            p.setImagem(personagem.getImagem());
                            tempArrayList.add(p);
                        }
                    }

                    int i = tempArrayList.size();
                    for (Personagem personagem : tempArrayList) {
                        personagem.setPosicao(i);
                        i--;
                    }
                    personagens = tempArrayList;
                    infinito = false;
                }else{
                    infinito = true;
                }

                dialog.dismiss();
                update();

                updateATB();
            }
        });

        dialog.show();


    }
}

## Sobre o projeto

Este projeto nasceu de uma ideia de utilizar os meios tecnologicos atuais para auxiliar partidas de RPG, a ideia é basicamente auxiliar a ordem em que as pessoas devem jogar, eliminando falhas como, pular a vez de um jogador, adiantar uma jogada dentre outros

Inicialmente este projeto foi planejado para a plataforma android, podendo futuramente ser desenvolvido para outras plataformas.

## Instalação

Por se tratar de um projeto que utiliza java, é necessário instalar o [JDK](http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html) para garantir o funcionamento dos passos a seguir 

 1. Instalar o [Android Studio](https://developer.android.com/studio/index.html)
 2. Obter uma versão do android superior a 4.0, recomendavel 6.0
 4. Instalar o [git] (https://git-scm.com/downloads)
 3. Clonar este repositório

### Clonando o repositório

Configurar o usuário

    git config --global user.name "Seu Nome"
    git config --global user.email "seu@email.com"

Clonar o repositório
No prompt/terminal:

    git clone https://gitlab.com/furflez/android-time-battle.git
    cd android-time-battle
    git pull origin master
    
Se tudo der certo, no Android studio após abrir o projeto, o graddle irá sincronizar os arquivos com uma mensagem de sucesso.

## Funcionalidades
| Funcionalidade        | Status              |
| ----------------------| :-----------------: |
| Adicionar Personagens | Estável             |
| Gerar ordem de batalha| Estável             |
| Criar ficha simples   | A desenvolver       |